%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: mskInvestorGestureOverride
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 0
  - m_Path: Armature/Root
    m_Weight: 0
  - m_Path: Armature/Root/ArmIK.L
    m_Weight: 1
  - m_Path: Armature/Root/ArmIK.L/ArmIK.L_end
    m_Weight: 1
  - m_Path: Armature/Root/ArmIK.R
    m_Weight: 1
  - m_Path: Armature/Root/ArmIK.R/ArmIK.R_end
    m_Weight: 1
  - m_Path: Armature/Root/ElbowTargetIK.L
    m_Weight: 1
  - m_Path: Armature/Root/ElbowTargetIK.L/ElbowTargetIK.L_end
    m_Weight: 1
  - m_Path: Armature/Root/ElbowTargetIK.R
    m_Weight: 1
  - m_Path: Armature/Root/ElbowTargetIK.R/ElbowTargetIK.R_end
    m_Weight: 1
  - m_Path: Armature/Root/FootIK.L
    m_Weight: 0
  - m_Path: Armature/Root/FootIK.L/FootIK.L_end
    m_Weight: 0
  - m_Path: Armature/Root/FootIK.R
    m_Weight: 0
  - m_Path: Armature/Root/FootIK.R/FootIK.R_end
    m_Weight: 0
  - m_Path: Armature/Root/KneeTargetIK.L
    m_Weight: 0
  - m_Path: Armature/Root/KneeTargetIK.L/KneeTargetIK.L_end
    m_Weight: 0
  - m_Path: Armature/Root/KneeTargetIK.R
    m_Weight: 0
  - m_Path: Armature/Root/KneeTargetIK.R/KneeTargetIK.R_end
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/SpineLower
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.L/ArmLower.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.L/ArmLower.L/WristSpinner.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.L/ArmLower.L/WristSpinner.L/CoinShooter.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.L/ArmLower.L/WristSpinner.L/CoinShooter.L/Antenna
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.L/ArmLower.L/WristSpinner.L/CoinShooter.L/Antenna/Antenna_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.L/ArmLower.L/WristSpinner.L/HumanHand.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.L/ArmLower.L/WristSpinner.L/HumanHand.L/FingerIndex01.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.L/ArmLower.L/WristSpinner.L/HumanHand.L/FingerIndex01.L/FingerIndex02.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.L/ArmLower.L/WristSpinner.L/HumanHand.L/FingerIndex01.L/FingerIndex02.L/FingerIndex02.L_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.L/ArmLower.L/WristSpinner.L/HumanHand.L/FingerMiddle01.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.L/ArmLower.L/WristSpinner.L/HumanHand.L/FingerMiddle01.L/FingerMiddle02.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.L/ArmLower.L/WristSpinner.L/HumanHand.L/FingerMiddle01.L/FingerMiddle02.L/FingerMiddle02.L_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.L/ArmLower.L/WristSpinner.L/HumanHand.L/FingerPinky01.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.L/ArmLower.L/WristSpinner.L/HumanHand.L/FingerPinky01.L/FingerPinky02.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.L/ArmLower.L/WristSpinner.L/HumanHand.L/FingerPinky01.L/FingerPinky02.L/FingerPinky02.L_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.L/ArmLower.L/WristSpinner.L/HumanHand.L/Thumb01.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.L/ArmLower.L/WristSpinner.L/HumanHand.L/Thumb01.L/Thumb02.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.L/ArmLower.L/WristSpinner.L/HumanHand.L/Thumb01.L/Thumb02.L/Thumb03.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.L/ArmLower.L/WristSpinner.L/HumanHand.L/Thumb01.L/Thumb02.L/Thumb03.L/Thumb03.L_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.R/ArmLower.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.R/ArmLower.R/WristSpinner.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.R/ArmLower.R/WristSpinner.R/CoinShooter.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.R/ArmLower.R/WristSpinner.R/CoinShooter.R/CoinShooter.R_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.R/ArmLower.R/WristSpinner.R/HumanHand.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.R/ArmLower.R/WristSpinner.R/HumanHand.R/FingerIndex01.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.R/ArmLower.R/WristSpinner.R/HumanHand.R/FingerIndex01.R/FingerIndex02.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.R/ArmLower.R/WristSpinner.R/HumanHand.R/FingerIndex01.R/FingerIndex02.R/FingerIndex02.R_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.R/ArmLower.R/WristSpinner.R/HumanHand.R/FingerMiddle01.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.R/ArmLower.R/WristSpinner.R/HumanHand.R/FingerMiddle01.R/FingerMiddle02.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.R/ArmLower.R/WristSpinner.R/HumanHand.R/FingerMiddle01.R/FingerMiddle02.R/FingerMiddle02.R_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.R/ArmLower.R/WristSpinner.R/HumanHand.R/FingerPinky01.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.R/ArmLower.R/WristSpinner.R/HumanHand.R/FingerPinky01.R/FingerPinky02.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.R/ArmLower.R/WristSpinner.R/HumanHand.R/FingerPinky01.R/FingerPinky02.R/FingerPinky02.R_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.R/ArmLower.R/WristSpinner.R/HumanHand.R/Thumb01.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.R/ArmLower.R/WristSpinner.R/HumanHand.R/Thumb01.R/Thumb02.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.R/ArmLower.R/WristSpinner.R/HumanHand.R/Thumb01.R/Thumb02.R/Thumb03.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/ArmUpper.R/ArmLower.R/WristSpinner.R/HumanHand.R/Thumb01.R/Thumb02.R/Thumb03.R/Thumb03.R_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/Neck
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/Neck/Head
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/Neck/Head/Head_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/Shoulder.L
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/Shoulder.L/Shoulder.L_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/Shoulder.R
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/SpineLower/SpineUpper/Shoulder.R/Shoulder.R_end
    m_Weight: 1
  - m_Path: Armature/Root/Pelvis/Thigh.L
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/Thigh.L/Calf.L
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/Thigh.L/Calf.L/Foot.L
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/Thigh.L/Calf.L/Foot.L/Foot.L_end
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/Thigh.R
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/Thigh.R/Calf.R
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/Thigh.R/Calf.R/Foot.R
    m_Weight: 0
  - m_Path: Armature/Root/Pelvis/Thigh.R/Calf.R/Foot.R/Foot.R_end
    m_Weight: 0
  - m_Path: InvestorBody
    m_Weight: 0
