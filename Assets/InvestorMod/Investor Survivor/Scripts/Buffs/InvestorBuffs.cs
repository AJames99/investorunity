﻿using System.Collections.Generic;
using System.Linq;
using Moonstorm;
using RoR2;
using RoR2.ContentManagement;
using UnityEngine;

namespace Investor.Buffs {
	public class InvestorBuffs : BuffModuleBase {
		public static InvestorBuffs Instance { get; set; }
		public static BuffDef[] LoadedInvestorBuffs { get => Investor.Modules.Assets.serialContentPack.buffDefs; }
		public override SerializableContentPack ContentPack { get; set; } = Investor.Modules.Assets.serialContentPack;

		public override void Init() {
			Instance = this;
			base.Init();
			Debug.Log("InvestorBuffs init.");
			InitializeBuffs();
		}

		public override IEnumerable<BuffBase> InitializeBuffs() {
			base.InitializeBuffs()
				.ToList()
				.ForEach(buff => AddBuff(buff, ContentPack));
			return null;
		}
	}
}