﻿using System;
using System.Collections;
using System.Collections.Generic;
using RoR2;
using Moonstorm;
using UnityEngine;
using Investor.Modules;

namespace Investor.Buffs {
    public class TaxEvasionBuff : BuffBase {
		public override BuffDef BuffDef { get; set; } = Modules.Assets.mainAssetBundle.LoadAsset<BuffDef>("TaxEvasionBuffDef");
		public static BuffDef buff;

		public override void Initialize() {
			buff = BuffDef;
		}

		public override void AddBehavior(ref CharacterBody body, int stack) {
			Debug.Log($"AddBehaviour on TaxEvasion, stack {stack}");
			body.AddItemBehavior<TaxEvasionBehaviour>(stack);
		}

		public class TaxEvasionBehaviour : CharacterBody.ItemBehavior, IStatItemBehavior, IOnIncomingDamageServerReceiver {
			[HideInInspector]
			public float accumulatedDamage;
			bool isTrackerEnabled;

			void Start() {
				Debug.Log("### Started evading taxes.");
				if (body.healthComponent) {
					HG.ArrayUtils.ArrayAppend(ref body.healthComponent.onIncomingDamageReceivers, this);
				}
				accumulatedDamage = 0;
				isTrackerEnabled = false;
			}

			void OnDestroy() {
				if (body.healthComponent) {
					// Should work if nothing is messing with the order of things in this array.
					int i = Array.IndexOf(body.healthComponent.onIncomingDamageReceivers, this);
					if (i > -1) {
						HG.ArrayUtils.ArrayRemoveAtAndResize(ref body.healthComponent.onIncomingDamageReceivers, body.healthComponent.onIncomingDamageReceivers.Length, i);
					}
				}

				Debug.Log($"### Tax evasion complete: Raw Damage taken: {accumulatedDamage}, gold given: {accumulatedDamage * StaticValues.taxEvasionDmgToMoneyRatio}.");
				body.master.GiveMoney((uint)RoR2.Run.instance.GetDifficultyScaledCost(Mathf.FloorToInt(accumulatedDamage * StaticValues.taxEvasionDmgToMoneyRatio)));
			}

			public void OnIncomingDamageServer(DamageInfo damageInfo) {
				accumulatedDamage += damageInfo.damage;
			}

			public void RecalculateStatsStart() {
				
			}

			public void RecalculateStatsEnd() {

			}
		}
	}
}