﻿using EntityStates;
using Investor.Modules.Components;

namespace Investor.SkillStates {
	public class BaseInvestorSkillState : BaseSkillState {
		protected InvestorController investorController;

		public override void OnEnter() {
			investorController = base.GetComponent<InvestorController>();
			base.OnEnter();
		}
	}
}