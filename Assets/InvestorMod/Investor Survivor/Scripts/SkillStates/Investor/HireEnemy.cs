﻿using System;
using System.Linq;
using RoR2;
using EntityStates;
using UnityEngine;
using Investor.Modules;
using RoR2.CharacterAI;

namespace Investor.SkillStates {
	public class HireEnemy : BaseState {
		Employ.EmployeeTargetInfo hireTarget;

		Modules.Components.InvestorController investorController;

		public HireEnemy(Employ.EmployeeTargetInfo currentTarget) : base() {
			this.hireTarget = currentTarget;
		}

		public override void OnEnter() {
			base.OnEnter();
			Debug.Log($"### Employment process: Hired enemy (Entered HireEnemy state) at {Time.fixedTime}s.");

			Util.PlaySound(HireEnemy.beginSoundString, base.gameObject);
			
			investorController = base.characterBody.GetComponent<Modules.Components.InvestorController>();
			if (!investorController) {
				outer.SetNextStateToMain();
				Debug.Log($"### Employment process: Investor Controller not found! Cancelling next frame - at {Time.fixedTime}s.");
			}
		}

		public override void OnExit() {
			base.OnExit();
			Util.PlaySound(HireEnemy.finishSoundString, base.gameObject);

			// Enemy is actually hired onto the team here:
			PerformEmployment();
			investorController.AddToManifest(this.hireTarget.body);
			base.GetModelAnimator().SetBool("isOnThePhone", false);
			Debug.Log($"### Employment process: Completed (HireEnemy OnExit) at {Time.fixedTime}s.");
		}

		void PerformEmployment() {
			if (hireTarget.body) {
				CharacterBody hireBody = hireTarget.body;
				CharacterMaster hireMaster = hireBody.master;
				if (!hireMaster || !hireBody)
					return;

				// Charge the player
				CharacterMaster ownerMaster = characterBody.master;
				ownerMaster.money = (uint)Mathf.FloorToInt(Mathf.Max(ownerMaster.money - (hireMaster.money * StaticValues.employSalaryCostMultiplier), 0));
				Debug.Log($"### Money owned by {hireBody.GetDisplayName()}: {hireMaster.money}");

				// Swap team
				TeamComponent teamComponent = hireTarget.body.teamComponent;
				hireMaster.teamIndex = teamComponent.teamIndex = characterBody.teamComponent.teamIndex;

				// Heal to full
				hireBody.healthComponent.HealFraction(1.0f, default(ProcChainMask));

				// Set minion and AI ownership
				hireMaster.minionOwnership.SetOwner(characterBody.master);
				AIOwnership aiOwnership = hireMaster.gameObject.GetComponent<AIOwnership>();
				if (aiOwnership) {
					aiOwnership.ownerMaster = characterBody.master;
				} else {
					aiOwnership = hireMaster.gameObject.AddComponent<AIOwnership>();
					if (aiOwnership) {
						aiOwnership.ownerMaster = characterBody.master;
					}
				}
				BaseAI baseAI = hireMaster.gameObject.GetComponent<BaseAI>();
				if (baseAI) {
					baseAI.leader.gameObject = characterBody.gameObject;
					baseAI.currentEnemy.Reset();
					baseAI.buddy.Reset();
					baseAI.customTarget.Reset();
					baseAI.enemyAttention = baseAI.targetRefreshTimer = baseAI.skillDriverUpdateTimer = 0f;

					// Find our employee someone else to shoot at that isn't on his team.
					baseAI.currentEnemy = new BaseAI.Target(baseAI);
					HurtBox newTargetHurtbox = FindNewEmployeeTarget(baseAI, 500.0f, true, true);
					if (newTargetHurtbox) {
						if (newTargetHurtbox.healthComponent)
							if (newTargetHurtbox.healthComponent.body)
								baseAI.currentEnemy.gameObject = newTargetHurtbox.healthComponent.body.gameObject;
					}
				}

				// Copy items, and copy equipment if the employee doesn't already have one (e.g. is an elite)
				if (hireBody.equipmentSlot.equipmentIndex == EquipmentIndex.None)
					hireMaster.inventory.CopyEquipmentFrom(characterBody.master.inventory);

				hireMaster.inventory.CopyItemsFrom(characterBody.master.inventory, Inventory.defaultItemCopyFilterDelegate);

				// Reset state machines to default to avoid friendly firing
				foreach (EntityStateMachine entityStateMachine in hireBody.GetComponents<EntityStateMachine>()) {
					entityStateMachine.SetNextStateToMain();
				}
			}
		}

		public override void FixedUpdate() {
			base.FixedUpdate();
			if (base.isAuthority && base.fixedAge >= abilityDurationBase) {
				base.GetModelAnimator().SetBool("isOnThePhone", false);
				this.outer.SetNextStateToMain();
			}
		}

		public override InterruptPriority GetMinimumInterruptPriority() {
			return InterruptPriority.Death;// was "Frozen"
		}

		private HurtBox FindNewEmployeeTarget(BaseAI employee, float maxDistance, bool full360Vision, bool filterByLoS) {
			if (!employee.body) {
				return null;
			}
			employee.enemySearch.viewer = employee.body;
			employee.enemySearch.teamMaskFilter = TeamMask.allButNeutral;
			employee.enemySearch.teamMaskFilter.RemoveTeam(employee.master.teamIndex);
			employee.enemySearch.sortMode = BullseyeSearch.SortMode.Distance;
			employee.enemySearch.minDistanceFilter = 0f;
			employee.enemySearch.maxDistanceFilter = maxDistance;
			employee.enemySearch.searchOrigin = employee.bodyInputBank.aimOrigin;
			employee.enemySearch.searchDirection = employee.bodyInputBank.aimDirection;
			employee.enemySearch.maxAngleFilter = (full360Vision ? 180f : 90f);
			employee.enemySearch.filterByLoS = filterByLoS;
			employee.enemySearch.RefreshCandidates();
			return employee.enemySearch.GetResults().FirstOrDefault<HurtBox>();
		}

		public static string beginSoundString;
		public static string finishSoundString;

		public static float abilityDurationBase;
		public static float animationDuration;
	}
}
