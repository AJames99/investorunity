﻿using UnityEngine;
using UnityEngine.Networking;
using RoR2;
using RoR2.Skills;
using RoR2.Projectile;
using EntityStates;
using Investor.Modules;
using AK.Wwise;
using AK;

namespace Investor.SkillStates {
	public class SpareChange : BaseSkillState, SteppedSkillDef.IStepSetter {
		
		public int hand;

		private Ray aimRay;

		private float duration;
		private float effectiveDelay;

		bool fired;

		//public static float damageCoefficient;

		public static float force;

		public static float baseDuration = 2f;
		public static float fireDelayLeft = 0.5f;
		public static float fireDelayRight  = 0.4f;

		public static float recoilAmplitude;
		public static float minSpread;
		public static float maxSpread;
		public static float spreadBloomValue;
		public static string fireCoinsSoundStr;
		public static string clothingShuffleSoundStr;

		[SerializeField]
		public GameObject projectilePrefab;

		void SteppedSkillDef.IStepSetter.SetStep(int i) {
			this.hand = i;
		}

		void FireCoins(string targetMuzzle) {
			bool left = hand == 0;
			
			Util.PlaySound(SpareChange.fireCoinsSoundStr, base.gameObject);
			
			if(left)
				base.AddRecoil(-0.4f * SpareChange.recoilAmplitude, -0.8f * SpareChange.recoilAmplitude, -0.3f * SpareChange.recoilAmplitude, -0.2f * SpareChange.recoilAmplitude);
			else
				base.AddRecoil(-0.4f * SpareChange.recoilAmplitude, -0.8f * SpareChange.recoilAmplitude, 0.2f * SpareChange.recoilAmplitude, 0.3f * SpareChange.recoilAmplitude);

			if (base.isAuthority) {
				float damage = base.damageStat * StaticValues.coinScatterDamageCoefficient * GetDamageMultiplierForNetWorth();
				//Debug.Log($"{damage} = {base.damageStat}_base * {StaticValues.coinScatterDamageCoefficient}_coeff * {GetDamageMultiplierForNetWorth()}_bonus ");

				for (int i = 0; i < StaticValues.coinCount; i++) {
					this.aimRay = base.GetAimRay();
					aimRay.direction = Util.ApplySpread(aimRay.direction, SpareChange.minSpread, SpareChange.maxSpread, 1f, 1f);
					ProjectileManager.instance.FireProjectile(this.projectilePrefab, aimRay.origin, Util.QuaternionSafeLookRotation(aimRay.direction), base.gameObject, damage, SpareChange.force, Util.CheckRoll(critStat, characterBody.master), DamageColorIndex.Default, null, -1f);
				}
				base.characterBody.AddSpreadBloom(SpareChange.spreadBloomValue);
			}
	}

		public override void OnEnter() {
			base.OnEnter();
			this.duration = SpareChange.baseDuration / this.attackSpeedStat;
			this.effectiveDelay = (hand == 0 ? SpareChange.fireDelayLeft : SpareChange.fireDelayRight) / this.attackSpeedStat;
			this.aimRay = base.GetAimRay();
			base.StartAimMode(this.aimRay, 3f, false);
			string animationStateName = (this.hand == 0) ? "ThrowLeft" : "ThrowRight";		
			base.PlayAnimation("Gesture, Additive", animationStateName, "Primary.playbackRate", this.duration);
			base.PlayAnimation("Gesture, Override", animationStateName, "Primary.playbackRate", this.duration);
			fired = false;

			Util.PlaySound(SpareChange.clothingShuffleSoundStr, base.gameObject);
		}
		public override void FixedUpdate() {
			base.FixedUpdate();
			if (base.fixedAge > effectiveDelay && !fired) {
				fired = true;
				FireCoins(this.hand == 0 ? "MuzzleLeft" : "MuzzleRight");
			}
			
			if (base.fixedAge < this.duration || !base.isAuthority) // Not ready to transition out yet.
				return;

			this.outer.SetNextStateToMain(); // Transition out
		}

		// https://www.desmos.com/calculator/b4xjx0iy9u
		float GetDamageMultiplierForNetWorth() {
			uint netWorth = base.characterBody.master.money;
			float netWorthInUnits = netWorth / (float)Run.instance.GetDifficultyScaledCost(StaticValues.netWorthOneUnit);
			float bonusMultiplier = (1f -
				(1f / (1f + (StaticValues.damageBonusPerUnitOfNetWorth * netWorthInUnits)))
			) * StaticValues.damageBonusMax;
			//Debug.Log($"Net worth in units: {netWorthInUnits}");
			Debug.Log($"{bonusMultiplier} = (1 - (1 / (1 + ({StaticValues.damageBonusPerUnitOfNetWorth} * {netWorthInUnits})))) * {StaticValues.damageBonusMax}");
			return 1 + bonusMultiplier;
		}

		public override InterruptPriority GetMinimumInterruptPriority() {
			return InterruptPriority.PrioritySkill;
		}
	}
}