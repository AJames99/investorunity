﻿using UnityEngine;
using RoR2;
using EntityStates;
using Investor.Modules;

namespace Investor.SkillStates {

	// Charge up the invest ability
	public class Invest : BaseSkillState {

		//public static GameObject crosshairOverridePrefab;
		public static float initialDelay = 0.25f;
		public static float animDuration = 1.0f;
		public static float chargeDuration;
		//private GameObject defaultCrosshairPrefab;
		InvestTracker tracker;
		bool wasCancelled;

		public override void OnEnter() {
			base.OnEnter();
			tracker = base.characterBody.GetComponent<InvestTracker>();
			
			if (characterBody.master.money < 1) {
				wasCancelled = true;
				outer.SetNextStateToMain();
			} else {
				tracker.BeginCharge(characterBody.master.money);
			}

			Debug.Log("### Invest::OnEnter");
		}

		public override void OnExit() {
			if(wasCancelled)
				activatorSkillSlot.AddOneStock();

			Debug.Log("### Invest::OnExit");

			base.OnExit();
		}

		public override void FixedUpdate() {
			base.FixedUpdate();

			if (base.isAuthority) {
				this.AuthorityFixedUpdate();
			}

			if (fixedAge < initialDelay)
				return;

			float tmp = Mathf.Clamp01((fixedAge - initialDelay) / chargeDuration);
			//Debug.Log($"Clamp01(({fixedAge} - {initialDelay}) / {chargeDuration}) = {tmp}");
			tracker.SetCharge(tmp);
			//base.characterBody.SetSpreadBloom(this.charge, true);
		}

		private void AuthorityFixedUpdate() {
			if (!ShouldKeepChargingAuthority()) {
				if (wasCancelled)
					outer.SetNextStateToMain();
				else
					outer.SetNextState(this.GetNextStateAuthority());
			}
		}

		protected virtual bool ShouldKeepChargingAuthority() {
			return base.IsKeyDownAuthority();
		}

		protected virtual EntityState GetNextStateAuthority() {
			return new InvestStartTrading();
		}

		public override InterruptPriority GetMinimumInterruptPriority() {
			return InterruptPriority.Frozen;
		}
	}

	public class InvestStartTrading : BaseSkillState {
		[HideInInspector]
		public float charge;
		InvestTracker tracker;

		bool sellProcess;
		public static float tradingEndDelay = 2.0f;
		float totalEarnings;
		public static string tradingEndSound;

		public override void OnEnter() {
			base.OnEnter();
			tracker = base.characterBody.GetComponent<InvestTracker>();
			tracker.StopCharge();
			float price = tracker.BeginTrading(StaticValues.investInitialStockValue);
			characterBody.master.money = (uint)Mathf.Max(Mathf.CeilToInt(characterBody.master.money - price), 0);
			sellProcess = false;
			totalEarnings = 0;
			base.PlayAnimation("Gesture, Override", "TapPhone");
			base.PlayAnimation("Gesture, Additive", "TapPhone");

			Debug.Log("### InvestStartTrading::OnEnter");
		}

		public override void OnExit() {
			characterBody.master.GiveMoney((uint)Mathf.Max(0, Mathf.CeilToInt(totalEarnings)));
			tracker.CloseUI();
			//Debug.Log("### InvestStartTrading::OnExit");
			base.OnExit();
		}

		public override void FixedUpdate() {
			base.FixedUpdate();

			if (sellProcess) {
				if(this.fixedAge >= tradingEndDelay + StaticValues.investTradingTime) {
					outer.SetNextStateToMain();
				}
			} else if (this.fixedAge >= StaticValues.investTradingTime) {
				totalEarnings = tracker.StopTrading();
				sellProcess = true;
				Util.PlaySound(tradingEndSound, base.gameObject);
			}
		}
	}
}