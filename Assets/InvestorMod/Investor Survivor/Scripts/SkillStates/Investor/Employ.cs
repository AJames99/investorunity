﻿using UnityEngine.Networking;
using UnityEngine;
using RoR2;
using EntityStates;
using System.Linq;
using EntityStates.Treebot.Weapon;
using EntityStates.Huntress;
using Investor.Modules.Components;

namespace Investor.SkillStates {
	public class Employ : AimThrowableBase {

		public static float delayAfterRelease;
		float delayTimer;

		public static float salaryHealingCoefficient;   // Heal up to X%

		public static string enterSoundString;

		public static string exitSoundString;

		protected bool isEmploying;
		bool wasCancelled;

		private AimMortarRain goodState;

		public struct EmployeeTargetInfo {
			public readonly HurtBox hurtBox;
			public readonly CharacterBody body;
			public readonly GameObject rootObject;
			public readonly Transform transformToIndicateAt;

			public EmployeeTargetInfo(HurtBox source, CharacterBody body) {
				this.hurtBox = source;
				this.rootObject = (this.hurtBox ? this.hurtBox.healthComponent.gameObject : null);
				this.transformToIndicateAt = (this.hurtBox ? this.hurtBox.transform : null);
				this.body = body;
			}
		}

		protected EmployeeTargetInfo currentTarget;

		private BullseyeSearch targetFinder = new BullseyeSearch();

		InvestorController investorController;

		public override void OnEnter() {

			// Copy the assets that AimMortarRain uses
			if (this.goodState == null)
				this.goodState = new AimMortarRain();

			this.arcVisualizerPrefab = goodState.arcVisualizerPrefab;
			this.endpointVisualizerPrefab = goodState.endpointVisualizerPrefab;
			this.projectilePrefab = goodState.projectilePrefab;

			wasCancelled = isEmploying = false;

			base.OnEnter();
			Util.PlaySound(Employ.enterSoundString, base.gameObject);

			investorController = base.characterBody.GetComponent<Modules.Components.InvestorController>();

			if (!investorController) {
				wasCancelled = true;
				outer.SetNextStateToMain();
			}
		}

		public override void OnExit() {
			base.OnExit();

			wasCancelled = true;
			if (currentTarget.body && characterBody) {
				if (currentTarget.body.master && characterBody.master) {
					if (characterBody.master.money >= currentTarget.body.master.money && !IsTeleporterBoss(currentTarget.body.master)) {
						wasCancelled = false;
						this.outer.SetNextState(new HireEnemy(currentTarget));
						Debug.Log($"### Employment process: Proceeding (Exiting Employ state to HireEnemy state) at {Time.fixedTime}s.");
					}
				}
			}
			
			if (!this.outer.destroying) {
				Util.PlaySound(Employ.exitSoundString, base.gameObject);
			}
			// Refund the stock if we were unable to perform it.
			if (wasCancelled) {
				Debug.Log($"### Employment process: Cancelled (Exiting Employ state) at {Time.fixedTime}s.");
				base.activatorSkillSlot.AddOneStock();
				base.GetModelAnimator().SetBool("isOnThePhone", false);
			}
		}

		public override void FixedUpdate() {
			//base.FixedUpdate();
			this.fixedAge += Time.fixedDeltaTime;

			if (base.isAuthority && !this.KeyIsDown() && base.fixedAge > this.minimumDuration && !wasCancelled && !isEmploying) {
				this.UpdateTrajectoryInfo(out this.currentTrajectoryInfo);

				// Start the employment process
				if (!currentTarget.body)
					wasCancelled = true;

				if (!wasCancelled) {
					if (!isEmploying) {
						isEmploying = true;
						delayTimer = Time.fixedTime + delayAfterRelease;
						base.PlayAnimation("Gesture, Override", "CallPhone", "Secondary.playbackRate", HireEnemy.animationDuration);
						base.PlayAnimation("Gesture, Additive", "CallPhone", "Secondary.playbackRate", HireEnemy.animationDuration);
						base.GetModelAnimator().SetBool("isOnThePhone", true);

						Debug.Log($"### Employment process: Started (RMB released) at {Time.fixedTime}s.");
					}
				} else {
					this.outer.SetNextStateToMain();
				}
			}

			if (!isEmploying) {

				// Gather the targeted enemy here.
				GatherProspectiveEmployee();

				// If the previous check failed, then check again for allies.
				//if (!currentTarget.hurtBox)
				//	GatherExistingEmployee();

				// If nobody was found at all, then remove our current highlight.
				if (!currentTarget.hurtBox)
					DestroyHighlight();
			} else {
				
				this.UpdateTrajectoryInfo(out this.currentTrajectoryInfo);
				if (Time.fixedTime >= delayTimer) {
					this.outer.SetNextStateToMain();
				}
			}

			investorController.SanitiseManifest();
		}

		public override void UpdateTrajectoryInfo(out TrajectoryInfo dest) {
			if (currentTarget.body) {
				dest = default(AimThrowableBase.TrajectoryInfo);
				dest.hitPoint = currentTarget.body.corePosition;
				dest.hitNormal = currentTarget.body.transform.up;
			} else {
				base.UpdateTrajectoryInfo(out dest);
			}
		}
		protected void OnNewTargetHighlighted(CharacterBody bodyNew, bool isAlreadyHired) {
			DestroyHighlight();
			HighlightTarget(bodyNew, isAlreadyHired);
		}

		void SetTarget(Employ.EmployeeTargetInfo target, bool isAlreadyHired) {
			// New target was found, highlight them
			if (target.body != currentTarget.body) {
				OnNewTargetHighlighted(target.body, isAlreadyHired); // TODO changeme
			}

			currentTarget = target;
		}

		void HighlightTarget(CharacterBody body, bool isAlreadyHired) {
			if (!characterBody.isLocalPlayer)
				return;

			if (body) {
				InvestorController.localHighlightTarget = body;
			}

			//string nameOfTarget = body ? body.GetDisplayName() : "null";
			//if (success)
			//	Debug.Log("### Highlighted " + nameOfTarget);
			//else
			//	Debug.LogWarning("### Failed to highlight " + nameOfTarget);
		}

		void DestroyHighlight() {
			if (!characterBody.isLocalPlayer)
				return;

			Debug.Log("### Destroyed highlight.");

			InvestorController.localHighlightTarget = null;
		}

		public override void FireProjectile() {
			HurtBox hurtBox = this.currentTarget.hurtBox;
			if (hurtBox) {
				//hurtBox.healthComponent.body;
				isEmploying = true;
				//this.InvalidateCurrentTarget();
			}
		}

		public void GatherProspectiveEmployee() {
			this.ConfigureTargetFinderForEnemies();
			foreach (HurtBox box in targetFinder.GetResults()) {
				//this.currentTarget = new Employ.EmployeeTargetInfo(targetFinder.GetResults().FirstOrDefault<HurtBox>());
				if (!box.healthComponent)
					continue;

				CharacterBody body = box.healthComponent.body;

				if (!body)
					continue;

				CharacterMaster master = body.master;

				if (!investorController.IsEmployed(body) && !IsTeleporterBoss(master) ) {
					SetTarget(new Employ.EmployeeTargetInfo(box, body), false);
				}
			}
		}

		public void GatherExistingEmployee() {
			this.ConfigureTargetFinderForFriendlies();
			foreach (HurtBox box in targetFinder.GetResults()) {
				if (!box.healthComponent)
					continue;

				CharacterBody body = box.healthComponent.body;

				if (!body)
					continue;

				if (investorController.IsEmployed(body)) {
					SetTarget(new Employ.EmployeeTargetInfo(box, body), true);
				}
			}
		}

		private void ConfigureTargetFinderBase() {
			this.targetFinder.teamMaskFilter = TeamMask.allButNeutral;
			this.targetFinder.teamMaskFilter.RemoveTeam(this.teamComponent.teamIndex);
			this.targetFinder.sortMode = BullseyeSearch.SortMode.Angle;
			this.targetFinder.filterByLoS = true;
			float rayDistExtra;
			Ray ray = CameraRigController.ModifyAimRayIfApplicable(this.GetAimRay(), base.gameObject, out rayDistExtra);
			this.targetFinder.searchOrigin = ray.origin;
			this.targetFinder.searchDirection = ray.direction;
			this.targetFinder.maxAngleFilter = 5f;
			this.targetFinder.viewer = this.characterBody;
		}

		private void ConfigureTargetFinderForEnemies() {
			this.ConfigureTargetFinderBase();
			this.targetFinder.teamMaskFilter = TeamMask.GetUnprotectedTeams(this.teamComponent.teamIndex);
			this.targetFinder.RefreshCandidates();
			this.targetFinder.FilterOutGameObject(base.gameObject);
		}

		private void ConfigureTargetFinderForFriendlies() {
			this.ConfigureTargetFinderBase();
			this.targetFinder.teamMaskFilter = TeamMask.none;
			this.targetFinder.teamMaskFilter.AddTeam(this.teamComponent.teamIndex);
			this.targetFinder.RefreshCandidates();
			this.targetFinder.FilterOutGameObject(base.gameObject);
		}

		private void InvalidateCurrentTarget() {
			this.currentTarget = default(Employ.EmployeeTargetInfo);
		}

		bool IsTeleporterBoss(CharacterMaster master) {
			if (master)
				return master.isBoss;
			else
				return false;
		}

		public override InterruptPriority GetMinimumInterruptPriority() {
			return InterruptPriority.PrioritySkill;
		}
	}
}