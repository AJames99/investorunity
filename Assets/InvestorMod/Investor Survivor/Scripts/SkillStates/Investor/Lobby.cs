﻿using UnityEngine.Networking;
using UnityEngine;
using RoR2;
using EntityStates;

namespace Investor.SkillStates {
	public class Lobby : BaseState {

		public static float delay = 1.5f;
		public static float animDuration = 1.0f;
		public static float radius = 500.0f;
		public static float procCoeff = 1.0f;
		//public static float decimateDamageCoefficient = 50.0f;
		public static float decimateCostCoefficient = 0.1f; // Of net worth
		public static float damagePerNetWorthBaseLevel = 5.0f;

		protected bool hasDecimated;
		protected bool wasCancelled;
		
		public override void OnEnter() {
			base.OnEnter();
			hasDecimated = false;
			if (base.characterBody.master.money < 1) {
				base.outer.SetNextStateToMain();
				wasCancelled = true;
			}
			base.PlayAnimation("Gesture, Override", "CallPhone", "Secondary.playbackRate", Lobby.animDuration);
		}
		
		public override void FixedUpdate() {
			base.FixedUpdate();
			if (base.fixedAge > Lobby.delay && !hasDecimated) {
				DecimateHealth();
			}
		}

		protected void DecimateHealth() {
			hasDecimated = true;
			BlastAttack blastAttack = new BlastAttack();
			blastAttack.radius = Lobby.radius;
			blastAttack.procCoefficient = Lobby.procCoeff;
			blastAttack.position = base.transform.position;
			blastAttack.attacker = base.gameObject;
			blastAttack.crit = Util.CheckRoll(base.characterBody.crit, base.characterBody.master);

			uint netWorth = base.characterBody.master.money;
			uint teamLevel = TeamManager.instance.GetTeamLevel(base.characterBody.master.teamIndex);
			float divisor = 1 + ((teamLevel - 1) * 0.25f);
			float damageTotal = base.characterBody.damage * ((damagePerNetWorthBaseLevel * netWorth) / divisor);
			Debug.Log("Lobby: base dmg " + base.characterBody.damage + "; " + damagePerNetWorthBaseLevel + " * " + netWorth + " / " + divisor + " = " + damageTotal);
			//blastAttack.baseDamage = base.characterBody.damage * Lobby.decimateDamageCoefficient;
			blastAttack.baseDamage = damageTotal;

			blastAttack.falloffModel = BlastAttack.FalloffModel.None;
			blastAttack.damageType = DamageType.BypassArmor | DamageType.AOE | DamageType.Stun1s;
			blastAttack.baseForce = 0.0f;
			blastAttack.teamIndex = TeamComponent.GetObjectTeam(blastAttack.attacker);
			blastAttack.attackerFiltering = AttackerFiltering.NeverHit;
			blastAttack.Fire();

			if (NetworkServer.active) {
				uint moneyToDeduct = (uint)Mathf.Min(Mathf.CeilToInt(base.characterBody.master.money * Lobby.decimateCostCoefficient), base.characterBody.master.money);
				base.characterBody.master.money -= moneyToDeduct;
			}

			base.outer.SetNextStateToMain();
		}

		public override InterruptPriority GetMinimumInterruptPriority() {
			return InterruptPriority.Skill;
		}

		public override void OnExit() {
			base.OnExit();

			// Refund the stock if we were unable to perform it.
			if (wasCancelled) {
				base.skillLocator.FindSkill("Lobby").AddOneStock();
			}
		}
	}
}