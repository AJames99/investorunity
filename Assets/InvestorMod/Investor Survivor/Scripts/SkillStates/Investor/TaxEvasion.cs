﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoR2;
using EntityStates;
using Moonstorm;
using Investor.Modules;
using Investor.Modules.Components;

namespace Investor.SkillStates {
	public class TaxEvasion : BaseState {

		public static BuffDef taxEvasionBuffPrimary;

		public static float taxEvasionActivationDelay;

		InvestorController investorController;

		bool hasActivated;

		public override void OnEnter() {
			base.OnEnter();
			base.PlayAnimation("Gesture, Override", "TapPhone");
			base.PlayAnimation("Gesture, Additive", "TapPhone");
			investorController = characterBody.GetComponent<InvestorController>();
			hasActivated = false;
		}

		public override void FixedUpdate() {
			base.FixedUpdate();
			if (this.fixedAge > taxEvasionActivationDelay && !hasActivated) {
				characterBody.AddTimedBuff(taxEvasionBuffPrimary, StaticValues.taxEvasionDuration);
				characterBody.AddTimedBuff(RoR2.RoR2Content.Buffs.CloakSpeed, StaticValues.taxEvasionDuration);
				if (investorController) {
					investorController.SummonWorkforce();
				}
				base.outer.SetNextStateToMain();
				hasActivated = true;
			}
		}



		public override InterruptPriority GetMinimumInterruptPriority() {
			return InterruptPriority.PrioritySkill;
		}
	}
}