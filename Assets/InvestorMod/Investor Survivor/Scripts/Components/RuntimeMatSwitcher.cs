﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RuntimeMatSwitcher : MonoBehaviour {
	[SerializeField]
	bool pathIsShader = false;

	[SerializeField]
	string assetPath;

	// Start is called before the first frame update
	void Start() {
		MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
		Image imageComponent = GetComponent<Image>();
		LineRenderer lineComponent = GetComponent<LineRenderer>();
		SkinnedMeshRenderer skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
		if (meshRenderer) {
			DoOperation<MeshRenderer>(meshRenderer, assetPath, pathIsShader);
		} else if (imageComponent) {
			DoOperation<Image>(imageComponent, assetPath, pathIsShader);
		} else if (lineComponent) {
			DoOperation<LineRenderer>(lineComponent, assetPath, pathIsShader);
		} else if (skinnedMeshRenderer) {
			DoOperation<SkinnedMeshRenderer>(skinnedMeshRenderer, assetPath, pathIsShader);
		} else {
			Debug.LogError($"### Unable to swap {(pathIsShader ? "shader" : "mat")} at runtime on object {name}! (Could not get any component)");
			return;
		}
	}

	static void DoOperation<T>(T component, string path, bool isShader) {
		if (isShader) {
			SwapShader<T>(component, path);
		} else {
			SwapMaterial<T>(component, path);
		}
	}

	static void SwapShader<T>(T component, string path) {
		//Shader shader = Resources.Load<Shader>(path);
		Shader shader = Shader.Find(path);
		if (shader) {
			if(component as Image) 
				(component as Image).material.shader = shader;
			else if (component as MeshRenderer)
				(component as MeshRenderer).material.shader = shader;
			else if (component as LineRenderer)
				(component as LineRenderer).material.shader = shader;
			else if (component as SkinnedMeshRenderer)
				(component as SkinnedMeshRenderer).material.shader = shader;

			Debug.Log($"Successfully swapped shader on {(component as Component).name} to {shader.name}.");
		} else {
			Debug.LogError($"### Unable to swap shader at runtime on object {(component as Component).name}! (Could not load specified shader at {path})");
		}
	}

	static void SwapMaterial<T>(T component, string path) {
		Material mat = Resources.Load<Material>(path);
		if (mat) {
			if (component as Image)
				(component as Image).material = mat;
			else if (component as MeshRenderer)
				(component as MeshRenderer).material = mat;
			else if (component as LineRenderer)
				(component as LineRenderer).material = mat;
			else if (component as SkinnedMeshRenderer)
				(component as SkinnedMeshRenderer).material = mat;

			Debug.Log($"Successfully swapped material on {(component as Component).name} to {mat.name}.");
		} else {
			Debug.LogError($"### Unable to swap mat at runtime on object {(component as Component).name}! (Could not load specified material at {path})");
		}
	}
}
