﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FontCopier : MonoBehaviour {
	[SerializeField]
	List<Text> textToReplace;
	private void Awake() {
		Font bombardier = Resources.Load<Font>("tmpfonts/fontsource/bombard_");
		Material mat = Resources.Load<Material>("tmpfonts/bombardier/tmpbombdropshadowoutlined");
		if (!bombardier || !mat)
			return;

		foreach (Text text in textToReplace) {
			text.font = bombardier;
			text.material = mat;
		}
	}
}
