﻿//#define DEBUGLOG

/*
	Colour palette:
		Ticker up:		76, 112, 81 RGB / 4C7051
		Ticker down:	181, 26, 40 RGB / B51A28
		Panel:			E0E9EE
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Investor.Modules;
using RoR2;

public class StockTickerUI : MonoBehaviour {

	public int bufferElementCount;
	public float bufferLengthSeconds;

	public float closeTime = 10.0f;

	#region conversions
	public float initialValue;
	public float naturalNoisePerSecondMin = -3.0f; 	// A random value between these
	public float naturalNoisePerSecondMax = 1.0f;		// two floats is chosen between each write to the buffer
	public float maxValueDeltaPerSecond = 10.0f; // The most the current value can move toward the target value each second
	public float maxValueDeltaPerSecondDisplay = 40.0f;
	#endregion

	public Gradient lineGradient;

	public Color backgroundColourAlpha;
	public Color backgroundColourBeta;

	public struct StockValue {
		public float time;
		public float value;
		public StockValue(float t, float v) {
			time = t;
			value = v;
		}
	}

	#region debugutils
	public class StockEvent {
		public enum EventType {
			DamageTaken,
			DamageDealt,
			Kill,
			Heal,
			Debuffed,
			Death
		}

		public EventType eventType;
		public float amount;

		public StockEvent(EventType type, float amount = 0) {
			this.eventType = type;
			this.amount = amount;
		}
	}
	#endregion

	List<StockValue> stockValueRecord;
	StockValue[] valueBuffer;
	int valueBufferIdx;

	float currentTime;

	private float _currentValue;
	float currentValue {
		get {
			return _currentValue;
		}
		set {
			_currentValue = Mathf.Max(value, 0);
		}
	}
	float _currentValueTarget;
	float currentValueTarget {
		get {
			return _currentValueTarget;
		}
		set {
			_currentValueTarget = Mathf.Max(value, 0);
		}
	}

	float currentDisplayValue;
	float currentDisplayTarget;

	const float difficultyScale = 1.0f;

	float bufferInterval;
	float bufferTimer;

	float currentNoiseRate;

	bool trading;
	bool currentArrowStatusIsUp;

	Mesh graphMesh;
	Mesh graphOutlineMesh;
	Mesh graphBackgroundMesh;

	[Header("Drag and Drop")]

	[SerializeField]
	MeshFilter targetMeshFilter;

	[SerializeField]
	MeshFilter backgroundMeshFilter;

	[SerializeField]
	LineRenderer outlineLineRenderer;

	[SerializeField]
	Camera chartRenderCamera;
	float cameraRadius;
	[SerializeField]
	float windowSizeSeconds = 5.0f;
	[SerializeField]
	float windowSizeVerticalPadding = 5.0f; // 5 units of padding vertical
	[SerializeField]
	float backgroundMeshOffset = 1;

	[SerializeField]
	Text stockValue;

	[SerializeField]
	Text stockName;

	[SerializeField]
	Text changeText;

	[SerializeField]
	Text percentageChangeText;

	[SerializeField]
	Animator arrowAnimator;

	[SerializeField]
	Image arrowUp;
	[SerializeField]
	Image arrowDown;

	[SerializeField]
	Text flavourText;
	[SerializeField]
	Text flavourTextShadow;

	[SerializeField]
	Animator flavourAnimator;

	[SerializeField]
	List<FlavourText> flavourTexts;

	[SerializeField, Range(0,1)]
	float flavourTextProbabilityMajor = 0.33f; // Applies to being debuffed and to getting kills

	[SerializeField, Range(0, 1)]
	float flavourTextProbabilityMinor = 0.15f; // Applies to taking/dealing/healing damage

	[SerializeField, Range(0, 10)]
	float flavourTextCooldown = 2.0f;
	float flavourTextTimer;

	[System.Serializable]
	public struct FlavourText {
		public enum Type {
			NEUTRAL,
			SMALLPOSITIVE,
			POSITIVE,
			SMALLNEGATIVE,
			NEGATIVE
		}
		public string text;
		public Type type;
	}
	Dictionary<FlavourText.Type, List<FlavourText>> availableFlavourTexts; // Has this flavour text been used recently? Only when all have been used at least once should it repeat any.
	string GetRandomFlavourText(FlavourText.Type flavourType) {
		// Check to see if all texts for this type have been used.
		if (availableFlavourTexts[flavourType].Count < 1) {
			// Reset all entries.
			foreach (FlavourText flavourText in flavourTexts) {
				if (flavourText.type == flavourType)
					availableFlavourTexts[flavourType].Add(flavourText);
			}
		}

		if (availableFlavourTexts[flavourType].Count < 1)
			return "null";

		int randomIdx = Random.Range(0, availableFlavourTexts[flavourType].Count);
		FlavourText randomTxt = availableFlavourTexts[flavourType][randomIdx];
		availableFlavourTexts[flavourType].RemoveAt(randomIdx);

		return randomTxt.text;
	}

	[SerializeField]
	InvestorProjectorManager projectorManager;

	[SerializeField]
	Animator tickerUIAnimator;

	[SerializeField]
	RectTransform chargeMeter;

	[SerializeField]
	Text chargeText;

	[SerializeField]
	Animator chargePanelAnimator;

	[SerializeField]
	Text sharesTextChargePopup;

	[SerializeField]
	Text sharesTextMain;

	[Header("Groups")]
	[SerializeField]
	Transform mainPanelTransform;
	[SerializeField]
	Transform graphPanelTransform;
	[SerializeField]
	Transform chargePanelTransform;

	float currentShares;

	void Start() {
		valueBuffer = new StockValue[bufferElementCount];
		stockValueRecord = new List<StockValue>(256);
		currentTime = 0;
		currentDisplayTarget = currentDisplayValue = currentValueTarget = currentValue = initialValue;

		bufferInterval = bufferLengthSeconds / (float)bufferElementCount;
		bufferTimer = 0f;
		valueBufferIdx = 0;

		currentNoiseRate = Random.Range(naturalNoisePerSecondMin, naturalNoisePerSecondMax);

		availableFlavourTexts = new Dictionary<FlavourText.Type, List<FlavourText>>();
		for (int i = (int)FlavourText.Type.NEUTRAL; i < (int)FlavourText.Type.NEGATIVE; i++) {
			availableFlavourTexts.Add((FlavourText.Type)i, new List<FlavourText>());
		}

		trading = false;

		flavourTextTimer = 0;

		// The main chart mesh
		graphMesh = new Mesh();
		targetMeshFilter.mesh = graphMesh;

		outlineLineRenderer.colorGradient = lineGradient;

		// Striped background
		graphBackgroundMesh = new Mesh();
		backgroundMeshFilter.mesh = graphBackgroundMesh;

		cameraRadius = chartRenderCamera.orthographicSize;

		// Render BG
		BuildGraphMesh();

		currentArrowStatusIsUp = true;
		arrowDown.gameObject.SetActive(false);
		arrowUp.gameObject.SetActive(false);
		UpdateDeltas(true);
		UpdateValue(true);

		currentShares = 0;

		HideInterface();

		// Disable for other network players here:

		SetAlphaOfChildren(chargePanelTransform, 0.0f);
	}

	public float StartTrading(float initialStockValue) {
		initialValue = initialStockValue;
		currentDisplayTarget = currentDisplayValue = currentValue = currentValueTarget = initialValue;
		stockValueRecord.Clear();
		currentTime = 0;
		bufferTimer = 0f;
		valueBufferIdx = 0;

		currentNoiseRate = Random.Range(naturalNoisePerSecondMin, naturalNoisePerSecondMax);

		graphMesh = new Mesh();
		targetMeshFilter.mesh = graphMesh;
		graphBackgroundMesh = new Mesh();
		backgroundMeshFilter.mesh = graphBackgroundMesh;

		flavourTextTimer = 0;

		BuildGraphMesh();

		currentArrowStatusIsUp = true;
		arrowDown.gameObject.SetActive(false);
		arrowUp.gameObject.SetActive(false);
		UpdateDeltas(true);
		UpdateValue(true);

		trading = true;

		return GetBuySharesPrice();
	}

	public void ShowInterface() {
		tickerUIAnimator.SetBool("isDeployed", true);
		projectorManager.gameObject.SetActive(true);
		SetAlphaOfChildren(mainPanelTransform, 1.0f);
		SetAlphaOfChildren(chargePanelTransform, 0.0f);
	}

	public void HideInterface() {
		tickerUIAnimator.SetBool("isDeployed", false);
		chargePanelAnimator.SetBool("isDeployed", false);
		projectorManager.gameObject.SetActive(false);
	}

	public void ClearUI() {
		currentDisplayTarget = currentDisplayValue = currentValue = currentValueTarget = 0;
		stockValueRecord.Clear();
		BuildGraphMesh();
		currentArrowStatusIsUp = true;
		arrowDown.gameObject.SetActive(false);
		arrowUp.gameObject.SetActive(false);
		UpdateDeltas(true);
		UpdateValue(true);
	}

	void Update() {
		if (!trading || PauseManager.isPaused)
			return;

		currentTime += Time.deltaTime;

		currentValueTarget += (currentNoiseRate * Time.deltaTime);
		LerpToTarget();

#if DEBUGLOG
		Debug.Log($"{currentTime}: {currentValue} -> {currentValueTarget}");
#endif

		if (currentTime >= bufferTimer) {
			bufferTimer = currentTime + bufferInterval;
			valueBuffer[valueBufferIdx] = new StockValue(currentTime, Mathf.Max(currentValue, 0));

			// Re-roll the decrease rate
			currentNoiseRate = Random.Range(naturalNoisePerSecondMin, naturalNoisePerSecondMax);
			
			if (++valueBufferIdx > bufferElementCount - 1) {
				valueBufferIdx = 0;
				WriteBufferToGraph();
			}
		}

		UpdateValue();
	}

	void LerpToTarget() {
		currentValue = GetLerpToTarget(currentValue, currentValueTarget, maxValueDeltaPerSecond);
	}

	static float GetLerpToTarget(float value, float target, float maxChangePerSecond) {
		if (Mathf.Abs(value - target) < Time.deltaTime * maxChangePerSecond) {
			return target;
		}

		if (target > value) {
			return value + maxChangePerSecond * Time.deltaTime;
		} else if (target < value) {
			return value - maxChangePerSecond * Time.deltaTime;
		}
		return value;
	}

	public void ExecuteEvent(StockEvent stockEvent) {
		switch (stockEvent.eventType) {
			case StockEvent.EventType.DamageDealt:
				currentValueTarget += stockEvent.amount * StaticValues.investDamageDealtToValueRatio;
				PollFlavourText(true, false);
				break;
			case StockEvent.EventType.DamageTaken:
				currentValueTarget -= stockEvent.amount * StaticValues.investDamageTakenToValueRatio;
				PollFlavourText(true, false);
				DoArrowBounce(false);
				break;
			case StockEvent.EventType.Heal:
				currentValueTarget += stockEvent.amount * StaticValues.investHealingToValueRatio;
				PollFlavourText(false, false);
				break;
			case StockEvent.EventType.Kill:
				currentValueTarget += StaticValues.investKillValue;
				PollFlavourText(false, true);
				DoArrowBounce(true);
				break;
			case StockEvent.EventType.Debuffed:
				currentValueTarget += StaticValues.investDebuffedValue;
				PollFlavourText(true, true);
				DoArrowBounce(false);
				break;
			case StockEvent.EventType.Death:
				currentValueTarget = 0;
				StopTrading();
				PollFlavourText(true, true);
				DoArrowBounce(false);
				break;
		}
	}

	void PollFlavourText(bool isNegative, bool isMajor) {
		if (Time.time < flavourTextTimer)
			return;

		flavourTextTimer = Time.time + flavourTextCooldown;
		if (Random.value > (isMajor ? flavourTextProbabilityMajor : flavourTextProbabilityMinor)) {

			FlavourText.Type eventType = isNegative ? (isMajor ? FlavourText.Type.NEGATIVE : FlavourText.Type.SMALLNEGATIVE) : (isMajor ? FlavourText.Type.POSITIVE : FlavourText.Type.SMALLPOSITIVE);
			flavourTextShadow.text = flavourText.text = GetRandomFlavourText(eventType);
			flavourAnimator.SetTrigger("Pop");
		}
	}

	void DoArrowBounce(bool upward) {
		if(currentArrowStatusIsUp == upward)
			arrowAnimator.SetTrigger(upward ? "BounceUp" : "BounceDown");
	}

	void WriteBufferToGraph() {
		stockValueRecord.AddRange(valueBuffer);

		// temp dbg
#if DEBUGLOG
		Debug.Log("-------------------------------------------------");
		for (int idx = stockValueRecord.Count - (bufferElementCount); idx < stockValueRecord.Count; idx++) {
			Debug.Log($"Value at time {stockValueRecord[idx].time.ToString("F2")}: {stockValueRecord[idx].value.ToString("F2")}");
		}
		Debug.Log("-------------------------------------------------");
#endif

		SetDisplayValueTarget(currentValueTarget);
		UpdateDeltas();

		BuildGraphMesh();
	}

	public float StopTrading() {
		trading = false;
		Debug.Log("TRADING FINISHED!");
		Debug.Log($"Initial value: {initialValue.ToString("F2")}, final value: {currentValue.ToString("F2")}, change: {(currentValue > initialValue ? "+" : "")}{(currentValue - initialValue).ToString("F2")}, {(((currentValue - initialValue) / ((currentValue + initialValue) / 2f)) * 100).ToString("F2")}%");

		currentDisplayTarget = currentValue = currentValueTarget;

		BuildGraphMesh();
		UpdateDeltas(true);
		UpdateValue(true);

		return currentDisplayTarget * currentShares;
	}

	void BuildGraphMesh() {
		float largestTValue = 0f;
		// Assume last entry is latest.
		if (stockValueRecord.Count > 0)
			largestTValue = stockValueRecord[stockValueRecord.Count - 1].time;

		float maxValueRecord = 10;
		float xAxisScale = (2 * cameraRadius) / windowSizeSeconds;
		float yAxisScale = 1.0f;

		if (stockValueRecord.Count > 1) {

			graphMesh.Clear();

			float minValueRecord = float.MaxValue;
			maxValueRecord = float.MinValue;
			for (int i = 0; i < stockValueRecord.Count; i++) {
				minValueRecord = Mathf.Min(stockValueRecord[i].value, minValueRecord);
				maxValueRecord = Mathf.Max(stockValueRecord[i].value, maxValueRecord);
			}

			//Debug.Log($"Of entire record: min value {minValueRecord}, max value {maxValueRecord}");

			// To fit the min and max values (+ some margin) of the past N seconds in the camera's view, we need to scale y-coord of verts appropriately:
			float minValueWindow = float.MaxValue, maxValueWindow = float.MinValue;
			for (int i = stockValueRecord.Count - 1; i >= 0; i--) {
				// Only check the last N seconds of entries
				if (largestTValue - stockValueRecord[i].time >= windowSizeSeconds)
					break;

				minValueWindow = Mathf.Min(minValueWindow, stockValueRecord[i].value);
				maxValueWindow = Mathf.Max(maxValueWindow, stockValueRecord[i].value);
			}
			yAxisScale = (2 * cameraRadius) /
				(
					(maxValueWindow + windowSizeVerticalPadding) - (Mathf.Max(minValueWindow - windowSizeVerticalPadding, 0f))
				);
			float yOffset = -((Mathf.Max(minValueWindow - windowSizeVerticalPadding, 0f)) * yAxisScale);   // Offset all vertices vertically so the minimum value of the past N seconds, post-scale, is at y=0
																										   // (+ windowPadding might need to be divided by 2)
																										   // To display the past N seconds, as defined by windowSizeSeconds, we need a multiplier for the x-coord of our verts:
			float xOffset = -xAxisScale * largestTValue;    // Offset all vertices so the final, rightmost verts are at x=0

			//Debug.Log($"Of window: min value {minValueWindow}, max value {maxValueWindow}, x-offset {xOffset}, y-offset {yOffset}");

			int dataPointCount = stockValueRecord.Count;

			// Main mesh buffers
			Vector3[] vertices = new Vector3[2 * dataPointCount];
			Vector3[] normals = new Vector3[2 * dataPointCount];
			int[] tris = new int[2 * (dataPointCount - 1) * 3];
			Vector2[] uv = new Vector2[2 * dataPointCount];
			// Outline mesh buffers
			Vector3[] verticesOutline = new Vector3[dataPointCount];
			int[] indicesOutline = new int[dataPointCount];

			// Used to determine the x-width for the UVs
			float maxWidth = stockValueRecord[dataPointCount - 1].time * xAxisScale;

			// Left-most data point:
			vertices[0] = new Vector3(xOffset + (stockValueRecord[0].time * xAxisScale), yOffset, 0); // "Base point"	(point on bottom/x-axis of graph)
			vertices[1] = new Vector3(xOffset + (stockValueRecord[0].time * xAxisScale), yOffset + (stockValueRecord[0].value * yAxisScale), 0); // "Value point" (plotted point above the baseline)
			uv[0] = new Vector2(0, 0);
			uv[1] = new Vector2(0, 1);
			normals[1] = normals[0] = -Vector3.forward;

			// Outline
			verticesOutline[0] = vertices[1];
			indicesOutline[0] = 0;

			// Iterate over all data points (bar the last one) and construct the subsequent point's verts, then make the tris, normals, and UVs between them.
			for (int idx = 0; idx < dataPointCount - 1; idx++) {
				int vertIdxBase = 2 * idx;
				int triIdxBase = 6 * idx;

				//Debug.Log($"{stockValueRecord[idx].time} : {stockValueRecord[idx].value}");

				// Assume current verts already exist,
				// vertIdxBase is current base, vertIdxBase + 1 is current value

				// Create next verts
				vertices[vertIdxBase + 2] = new Vector3(xOffset + (stockValueRecord[idx + 1].time * xAxisScale), yOffset, 0); // Next base point
				vertices[vertIdxBase + 3] = new Vector3(xOffset + (stockValueRecord[idx + 1].time * xAxisScale), yOffset + (stockValueRecord[idx + 1].value * yAxisScale), 0); // Next value point

				// Set outline point to just be the top edge of the mesh 
				verticesOutline[idx + 1] = vertices[vertIdxBase + 3];
				indicesOutline[idx + 1] = idx + 1;

				tris[triIdxBase] = vertIdxBase;     // Base
				tris[triIdxBase + 1] = vertIdxBase + 1; // Value
				tris[triIdxBase + 2] = vertIdxBase + 2; // Base next

				tris[triIdxBase + 3] = vertIdxBase + 1;     // Value
				tris[triIdxBase + 4] = vertIdxBase + 3;     // Value Next
				tris[triIdxBase + 5] = vertIdxBase + 2;     // Base Next

				// Normals are per-vertex and not per-face!
				normals[vertIdxBase + 3] = normals[vertIdxBase + 2] = -Vector3.forward;

				// X-coord of next slice's UVs will be the proportion that they are along the entire data set.
				float uvx = (stockValueRecord[idx + 1].time * xAxisScale) / maxWidth;
				uv[vertIdxBase + 2] = new Vector2(uvx, 0);
				uv[vertIdxBase + 3] = new Vector2(uvx, 1);

				// Colour based on how this vert's value compares to the record's max and min
				// Both base and value verts are the same colour
				//vertColours[vertIdxBase + 3] = vertColours[vertIdxBase + 2] = GetColourForValue(minValueRecord, maxValueRecord, stockValueRecord[idx+1].value);
			}

			graphMesh.vertices = vertices;
			graphMesh.triangles = tris;
			graphMesh.uv = uv;
			graphMesh.normals = normals;

			outlineLineRenderer.positionCount = verticesOutline.Length; // Fuck Unity.
			outlineLineRenderer.SetPositions(verticesOutline);
		}

		int segmentCount = Mathf.CeilToInt(Mathf.Max(largestTValue, windowSizeSeconds) / bufferLengthSeconds);
		float segmentWidth = bufferLengthSeconds * xAxisScale;
		float segmentYTop = (maxValueRecord * yAxisScale) + windowSizeVerticalPadding;
		float segmentYBottom = -windowSizeVerticalPadding;
		Vector3[] backgroundVerts = new Vector3[4 * segmentCount];
		int[] backgroundTris = new int[6 * segmentCount];
		Vector3[] backgroundNrms = new Vector3[4 * segmentCount];
		Color[] backgroundClrs = new Color[4 * segmentCount];
		float xOffsetBG = -xAxisScale * Mathf.Max(largestTValue, windowSizeSeconds);

		// Now the background slices
		for (int i = 0; i < segmentCount; i++) {
			float xLeft = i * segmentWidth, xRight = xLeft + segmentWidth;
			int vertIdxBase = 4 * i;
			int triIdxBase = 6 * i;

			backgroundVerts[vertIdxBase] = new Vector3(xLeft + xOffsetBG, segmentYBottom, backgroundMeshOffset); // Bottom left
			backgroundVerts[vertIdxBase + 1] = new Vector3(xLeft + xOffsetBG, segmentYTop, backgroundMeshOffset); // Top left
			backgroundVerts[vertIdxBase + 2] = new Vector3(xRight + xOffsetBG, segmentYTop, backgroundMeshOffset); // Top right
			backgroundVerts[vertIdxBase + 3] = new Vector3(xRight + xOffsetBG, segmentYBottom, backgroundMeshOffset); // Bottom right

			// [0,1,2] and [0,2,3]
			backgroundTris[triIdxBase] = vertIdxBase;
			backgroundTris[triIdxBase + 1] = vertIdxBase + 1;
			backgroundTris[triIdxBase + 2] = vertIdxBase + 2;

			backgroundTris[triIdxBase + 3] = vertIdxBase;
			backgroundTris[triIdxBase + 4] = vertIdxBase + 2;
			backgroundTris[triIdxBase + 5] = vertIdxBase + 3;

			backgroundClrs[vertIdxBase] = backgroundClrs[vertIdxBase + 1] = backgroundClrs[vertIdxBase + 2] = backgroundClrs[vertIdxBase + 3] = i % 2 == 0 ? backgroundColourAlpha : backgroundColourBeta;

			backgroundNrms[vertIdxBase] = backgroundNrms[vertIdxBase + 1] = backgroundNrms[vertIdxBase + 2] = backgroundNrms[vertIdxBase + 3] = -Vector3.forward;
		}


		graphBackgroundMesh.vertices = backgroundVerts;
		graphBackgroundMesh.triangles = backgroundTris;
		graphBackgroundMesh.normals = backgroundNrms;
		graphBackgroundMesh.colors = backgroundClrs;

		chartRenderCamera.Render();
	}

	void UpdateDeltas(bool forceRefresh = false) {
		float changeRaw, changePercentage;
		changeRaw = (currentValue - initialValue);
		//changePercentage = (((currentValue - initialValue) / ((currentValue + initialValue) / 2f)) * 100); // THIS IS PERCENTAGE *DIFFERENCE* NOT PERCENTAGE CHANGE U DOLT!!!
		changePercentage = (currentValue - initialValue) / Mathf.Abs(initialValue) * 100;
		changeText.text = FormatFloat(changeRaw);
		percentageChangeText.text = string.Concat(FormatFloat(changePercentage), "%");
		bool arrowIsUp = changeRaw >= 0;
		if (arrowIsUp != currentArrowStatusIsUp || forceRefresh) {
			arrowDown.gameObject.SetActive(!arrowIsUp);
			arrowUp.gameObject.SetActive(arrowIsUp);
			arrowAnimator.SetTrigger("Pop");

			changeText.color = arrowIsUp ? arrowUp.color : arrowDown.color;
			percentageChangeText.color = arrowIsUp ? arrowUp.color : arrowDown.color;

			currentArrowStatusIsUp = arrowIsUp;
		}
	}


	void SetDisplayValueTarget(float newTarget) {
		currentDisplayTarget = newTarget;
	}

	void UpdateValue(bool forceSnap = false) {
		currentDisplayValue = GetLerpToTarget(currentDisplayValue, currentDisplayTarget, maxValueDeltaPerSecondDisplay);
		if (forceSnap)
			currentDisplayTarget = currentDisplayValue = currentValueTarget;

		stockValue.text = FormatFloat(currentDisplayValue);
	}

	public void ShowChargeMeter(float initialValue, float initialPercent) {
		ShowInterface();
		chargePanelAnimator.SetBool("isDeployed", true);
		chargeText.text = string.Concat("$", FormatFloat(initialValue));
		chargeMeter.localScale = new Vector3(initialPercent, 1, 1);
		SetAlphaOfChildren(mainPanelTransform, 0.1f);
		SetAlphaOfChildren(graphPanelTransform, 0.1f);
		SetAlphaOfChildren(chargePanelTransform, 1.0f);
	}

	public void HideChargeMeter() {
		chargePanelAnimator.SetBool("isDeployed", false);
		SetAlphaOfChildren(mainPanelTransform, 1.0f);
		SetAlphaOfChildren(graphPanelTransform, 1.0f);
		SetAlphaOfChildren(chargePanelTransform, 0.1f);
	}

	// charge: 0 to 1 value representing how much of the user's net worth is being invested.
	// maxinvestment: the net worth of the user
	public void SetShares(float charge, float maxInvestment, float oneShare) {
		currentShares = (charge * maxInvestment) / oneShare;
		sharesTextChargePopup.text = string.Concat("(", FormatFloat(currentShares), " shares)");
		sharesTextMain.text = string.Concat(FormatFloat(currentShares), " shares");

		chargeText.text = string.Concat("$", FormatFloat(currentShares * oneShare));
		chargeMeter.localScale = new Vector3(charge, 1, 1);

		//Debug.Log($"### StockTickerUI::SetShares({charge}, {oneShare})");
	}

	public float GetBuySharesPrice() {
		return currentShares * initialValue;
	}

	static float Remap(float value, float low1, float high1, float low2, float high2) {
		return low2 + (value - low1) * (high2 - low2) / (high1 - low1);
	}

	string FormatFloat(float number) {
		//return number.ToString($"#.00");
		return number.ToString($"0.00");
	}

	// Recursively set the alpha of a component and its children.
	public void SetAlphaOfChildren(Transform transform, float alpha) {
		Image image = transform.GetComponent<Image>();
		Text text = transform.GetComponent<Text>();
		RawImage rawImage = transform.GetComponent<RawImage>();
		if (image) {
			image.color = ((image.color) * new Color(1, 1, 1, 0)) + new Color(0,0,0, alpha);
		} else if (text) {
			text.color = ((text.color) * new Color(1, 1, 1, 0)) + new Color(0, 0, 0, alpha);
		} else if(rawImage) {
			rawImage.color = ((rawImage.color) * new Color(1, 1, 1, 0)) + new Color(0, 0, 0, alpha);
		}

		if (transform.childCount > 0) {
			for (int i = 0; i < transform.childCount; i++) {
				SetAlphaOfChildren(transform.GetChild(i), alpha);
			}
		}
	}
}
