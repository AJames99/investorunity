﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoR2;
using Investor.Modules;

public class InvestTracker : MonoBehaviour, IOnDamageDealtServerReceiver, IOnTakeDamageServerReceiver, IOnKilledOtherServerReceiver, IOnKilledServerReceiver {
	bool isTrading;
	[SerializeField]
	StockTickerUI tickerUIManager;

	float maxInvestment;
	
	private void Start() {
		isTrading = false;
		maxInvestment = 0;
	}

	public float BeginTrading(float initialValue) {
		isTrading = true;
		float price = tickerUIManager.StartTrading(initialValue);
		StopCharge(); // Just in case
		//Debug.Log($"### InvestTracker::BeginTrading({initialValue}), price: {price}");
		return price;
	}

	public float StopTrading() {
		isTrading = false;
		//Debug.Log("### InvestTracker::StopTrading");
		return tickerUIManager.StopTrading();
	}

	public void CloseUI() {
		tickerUIManager.HideInterface();
		//Debug.Log("### InvestTracker::CloseUI");
	}

	public void BeginCharge(float maxInvestment) {
		tickerUIManager.ShowInterface();
		tickerUIManager.ClearUI();
		this.maxInvestment = maxInvestment;
		tickerUIManager.ShowChargeMeter(0f, 0f);
		//Debug.Log($"### InvestTracker::BeginCharge()");
	}

	public void SetCharge(float charge) {
		tickerUIManager.SetShares(charge, maxInvestment, StaticValues.investInitialStockValue);
		//Debug.Log($"### InvestTracker::SetCharge({charge})");
	}

	public void StopCharge() {
		tickerUIManager.HideChargeMeter();
		//Debug.Log("### InvestTracker::StopCharge");
	}

	public void OnTakeDamageServer(DamageReport damageReport) {
		if(isTrading)
			tickerUIManager.ExecuteEvent(new StockTickerUI.StockEvent(StockTickerUI.StockEvent.EventType.DamageTaken, damageReport.damageDealt));
	}

	public void OnDamageDealtServer(DamageReport damageReport) {
		if (isTrading)
			tickerUIManager.ExecuteEvent(new StockTickerUI.StockEvent(StockTickerUI.StockEvent.EventType.DamageDealt, damageReport.damageDealt));
	}

	public void OnKilledOtherServer(DamageReport damageReport) {
		if (isTrading)
			tickerUIManager.ExecuteEvent(new StockTickerUI.StockEvent(StockTickerUI.StockEvent.EventType.Kill));
	}

	public void OnKilledServer(DamageReport damageReport) {
		if (isTrading)
			tickerUIManager.ExecuteEvent(new StockTickerUI.StockEvent(StockTickerUI.StockEvent.EventType.Death));
	}
}
