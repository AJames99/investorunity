﻿using RoR2;
using RoR2.Skills;
using UnityEngine;
using System.Collections.Generic;
using EntityStates.Treebot.Weapon;
using Investor.SkillStates;
using System;
using RoR2.CharacterAI;
using RoR2.Navigation;

namespace Investor.Modules.Components {
	// just a class to run some custom code for things like weapon models
	// Attached as a component onto InvestorBody.
	public class InvestorController : MonoBehaviour {
		private CharacterBody characterBody;
		private CharacterModel model;
		private ChildLocator childLocator;
		private Animator modelAnimator;

		[HideInInspector]
		public int primaryAttackArm;

		//[SerializeField]
		//EntityStateMachine dummyStateMachine;

		List<CharacterBody> employeeManifest = new List<CharacterBody>();

		[HideInInspector]
		public static CharacterBody localHighlightTarget;

		//bool employInit;

		GameObject helperPrefab;

		static InvestorController() {
			OutlineHighlight.onPreRenderOutlineHighlight = (Action<OutlineHighlight>)Delegate.Combine(OutlineHighlight.onPreRenderOutlineHighlight, new Action<OutlineHighlight>(InvestorController.OnPreRenderOutlineHighlight));

			Debug.Log("### SETUP OnPreRenderOutlineHighlight!");
		}

		public bool IsEmployed(CharacterBody body) {
			return employeeManifest.Contains(body);
		}

		public void ClearManifest() {
			employeeManifest.Clear();
		}

		public void AddToManifest(CharacterBody body) {
			employeeManifest.Add(body);

			// TODO: go through and force all employees to find a new target
			foreach (CharacterBody employeeBody in employeeManifest) {
				if (employeeBody) {
					CharacterMaster employeeMaster = employeeBody.master;
					if (employeeMaster) {
						BaseAI baseAI = employeeMaster.gameObject.GetComponent<BaseAI>();
						if (baseAI) {
							baseAI.currentEnemy.Reset();
							//baseAI.buddy.Reset();
							//baseAI.customTarget.Reset();
							//baseAI.enemyAttention = baseAI.targetRefreshTimer = baseAI.skillDriverUpdateTimer = 0f;

							// Find our employee someone else to shoot at that isn't on his team.
							//baseAI.currentEnemy = new BaseAI.Target(baseAI);
							//HurtBox newTargetHurtbox = FindNewEmployeeTarget(baseAI, 500.0f, true, true);
							//if (newTargetHurtbox) {
							//    if (newTargetHurtbox.healthComponent)
							//        if (newTargetHurtbox.healthComponent.body)
							//            baseAI.currentEnemy.gameObject = newTargetHurtbox.healthComponent.body.gameObject;
							//}
						}
					}
				}
			}

			SanitiseManifest();
		}

		public void SummonWorkforce() {
			SanitiseManifest();

			//RoR2.CombatDirector.
			foreach (CharacterBody employeeBody in employeeManifest) {
				/*
				NodeGraph nodeGraph = SceneInfo.instance.GetNodeGraph(employeeBody.isFlying ? MapNodeGroup.GraphType.Air : MapNodeGroup.GraphType.Ground);
				NodeGraph.NodeIndex nodeIndex = nodeGraph.FindClosestNode(characterBody.corePosition, employeeBody.hullClassification, 150f);
				Vector3 nodePosition;
				if (nodeGraph.GetNodePosition(nodeIndex, out nodePosition)) {
					employeeBody.
				}
				*/

				SpawnCard spawnCard = ScriptableObject.CreateInstance<SpawnCard>();
				spawnCard.hullSize = employeeBody.hullClassification;
				spawnCard.nodeGraphType = (employeeBody.isFlying ? MapNodeGroup.GraphType.Air : MapNodeGroup.GraphType.Ground);
				spawnCard.prefab = this.helperPrefab;
				GameObject spawnAttempObject = DirectorCore.instance.TrySpawnObject(new DirectorSpawnRequest(spawnCard, new DirectorPlacementRule {
					placementMode = DirectorPlacementRule.PlacementMode.NearestNode,
					position = characterBody.corePosition + characterBody.characterMotor.velocity
				}, RoR2Application.rng));
				if (spawnAttempObject) {
					Vector3 spawnPosition = spawnAttempObject.transform.position;
					if ((spawnPosition - characterBody.corePosition).sqrMagnitude < 160000f) {
						Debug.Log($"Teleporting workforce member {employeeBody.GetDisplayName()}!");
						TeleportHelper.TeleportBody(employeeBody, spawnPosition);
						GameObject teleportEffectPrefab = Run.instance.GetTeleportEffectPrefab(employeeBody.gameObject);
						if (teleportEffectPrefab) {
							EffectManager.SimpleEffect(teleportEffectPrefab, spawnPosition, Quaternion.identity, true);
						}
						UnityEngine.Object.Destroy(spawnAttempObject);
					}
				}
				UnityEngine.Object.Destroy(spawnCard);
			}
		}

		public void SanitiseManifest() {
			List<CharacterBody> manifestCopy = new List<CharacterBody>(employeeManifest);
			foreach (CharacterBody body in employeeManifest) {
				if (!body) {
					manifestCopy.Remove(body);
				} else if (!body.healthComponent.alive) {
					manifestCopy.Remove(body);
				}
			}
			employeeManifest = manifestCopy;
		}


		AimMortarRain tempRain;

		private void Start() {
			this.helperPrefab = Resources.Load<GameObject>("SpawnCards/HelperPrefab");
		}

		private void Awake() {
			characterBody = gameObject.GetComponent<CharacterBody>();
			childLocator = gameObject.GetComponentInChildren<ChildLocator>();
			model = gameObject.GetComponentInChildren<CharacterModel>();
			modelAnimator = gameObject.GetComponentInChildren<Animator>();

			// well that's hacky.
			Invoke("CheckWeapon", 0.2f);
		}

		// Run as an Invoke to give time for everything else to be set up and sorted.
		private void CheckWeapon() {

			// TODO do the coin-shooter wrist bone check here
			//if (characterBody.skillLocator.secondary.skillDef.skillNameToken == InvestorPlugin.developerPrefix + "_THUNDERHENRY_BODY_SECONDARY_UZI_NAME") {
			//    childLocator.FindChild("GunModel").GetComponent<SkinnedMeshRenderer>().sharedMesh = Modules.Assets.mainAssetBundle.LoadAsset<Mesh>("meshUzi");
			//}
		}

		// For drawing highlighted employees/prospective employees
		private static void OnPreRenderOutlineHighlight(OutlineHighlight outlineHighlight) {

			if (!localHighlightTarget)
				return;

			if (!outlineHighlight.sceneCamera) {
				return;
			}
			if (!outlineHighlight.sceneCamera.cameraRigController) {
				return;
			}

			GameObject gameObject = localHighlightTarget.gameObject;

			Highlight targetedHighlight = gameObject.GetComponent<Highlight>();
			if (!targetedHighlight) {
				return;
			}

			Color highlightColour = targetedHighlight.GetColor();
			highlightColour = new Color(1.0f, 1.0f, 0.0f, 1.0f);
			outlineHighlight.highlightQueue.Enqueue(new OutlineHighlight.HighlightInfo {
				renderer = targetedHighlight.targetRenderer,
				color = highlightColour * targetedHighlight.strength
			});

			Debug.Log($"Targetd object highlight strength: {targetedHighlight.strength}");
		}
	}
}
