﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoR2;

public class InvestorProjectorManager : MonoBehaviour {
	[SerializeField]
	RectTransform targetPanel;
	[SerializeField]
	float projectorEndScaleFactor = 0.95f;
	[SerializeField]
	Vector3 projectorEndOffset = new Vector3(0,0, -1);
	[SerializeField]
	RectTransform panelToCopySizeFrom;
	[SerializeField]
	Transform projectorEndBone;
	[SerializeField]
	Transform projectorBaseBone;
	[SerializeField]
	Transform baseTransformTarget;
	[SerializeField]
	Vector3 baseBoneOffset = new Vector3(0.0f, 0.05f, 0.0f);

	[SerializeField]
	SkinnedMeshRenderer projectorMesh;

	[SerializeField]
	float alphaBoostMatParam = 0.4f;

	//float flickerTimer = 2.0f;

	private void Awake() {
		projectorMesh.material.SetFloat("_AlphaBoost", alphaBoostMatParam);
		projectorMesh.material.SetFloat("_VertexColorOn", 1f);
	}

	// Update is called once per frame
	void Update() {
		if (PauseManager.isPaused)
			return;

		projectorEndBone.position = targetPanel.position + (targetPanel.rotation * projectorEndOffset);
		projectorBaseBone.rotation = projectorEndBone.rotation = targetPanel.rotation;
		projectorEndBone.localScale = new Vector3(projectorEndScaleFactor * panelToCopySizeFrom.localScale.x * panelToCopySizeFrom.rect.width / 100f, projectorEndScaleFactor * panelToCopySizeFrom.localScale.y * panelToCopySizeFrom.rect.height / 100f, 1.0f);
		projectorBaseBone.position = baseTransformTarget.position + (baseTransformTarget.rotation * baseBoneOffset);
		//projectorEndBone.localScale = targetPanel.localScale;
		//projectorMesh.material.SetFloat("_AlphaBoost", 20 * (Mathf.Sin(Time.time / 20f) + 1) / 2);
		//projectorMesh.material.SetFloat("_AlphaBoost", Random.Range(0.0f, 20.0f));
	}
}
