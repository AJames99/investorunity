﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoR2;

public class LookAtMainCamera : MonoBehaviour {
	[SerializeField]
	AnimationCurve speedMultiplierCurve;
	[SerializeField]
	float maxInterpSpeed = 15.0f; // Base degrees per second

	[SerializeField]
	Vector2 angleDeltaRange;

	Transform mainCamTransf = null;

	// Start is called before the first frame update
	void Awake() {
		if(null != LocalUserManager.GetFirstLocalUser())
			if(null != LocalUserManager.GetFirstLocalUser().cameraRigController)
				mainCamTransf = LocalUserManager.GetFirstLocalUser().cameraRigController.transform;
	}

	// Update is called once per frame
	void Update() {
		LookAtSmooth();
	}

	// An additive lookat function
	void LookAtSmooth() {
		if (!mainCamTransf && null != LocalUserManager.GetFirstLocalUser()) {
			mainCamTransf = LocalUserManager.GetFirstLocalUser().cameraRigController.transform;
		}

		Quaternion start = Quaternion.Euler(transform.rotation.eulerAngles); // Copy current rot by val
		transform.LookAt(mainCamTransf.position, Vector3.up);
		Quaternion end = transform.rotation;
		float angleDifference = Quaternion.Angle(start, end);
		float angleDifference01 = RemapClamped(angleDifference, angleDeltaRange.x, angleDeltaRange.y, 0, 1);
		float rotateSpeed = (speedMultiplierCurve.Evaluate(angleDifference01) * maxInterpSpeed) * Time.deltaTime;
		if (angleDifference <= rotateSpeed)
			return; // Snap

		//Debug.Log($"### Angle Difference: {angleDifference}");

		transform.rotation = Quaternion.RotateTowards(start, end, rotateSpeed);
	}

	static float RemapClamped(float value, float from1, float to1, float from2, float to2) {
		return Mathf.Clamp(
			(((value - from1) / (to1 - from1)) * (to2 - from2)) + from2,
			Mathf.Min(from2, to2),
			Mathf.Max(from2, to2));
	}
}
