﻿using System;

namespace Investor.Modules
{
    // StaticValues is a good place to put in any variables you might want to change at a moment's notice
    // good for easily making balance changes. Usually you'd have the body values (health, movement speed, etc) be here too,
    // but that's in the CharacterBody component instead.
    internal static class StaticValues
    {
        // Spare Change
        internal const int coinCount = 8;
        internal const float coinScatterDamageCoefficient = 0.5f;
        internal const float damageBonusPerUnitOfNetWorth = 0.01f; // Maths-wise, functions the same as the %age chance of a single Tougher Times blocking damage.
        internal const float damageBonusMax = 4.0f;                 // What the hyperbolic function tends towards.
        internal const int netWorthOneUnit = 25;             // One "unit" here is the price of a small chest
        // https://www.desmos.com/calculator/b4xjx0iy9u

        // Tax Evasion
        internal const float taxEvasionDmgToMoneyRatio = 0.1f;
        internal const float taxEvasionDuration = 6.0f;
        
        // Employ
        internal const float employSalaryCostMultiplier = 1.0f;

        // Invest
        internal const float investKillValue = 12;
        internal const float investDamageDealtToValueRatio = 0.45f;
        internal const float investDamageTakenToValueRatio = 0.75f;
        internal const float investHealingToValueRatio = 0.75f;
        internal const float investDebuffedValue = -15;
        internal const float investInitialStockValue = 35f;
        internal const float investTradingTime = 12.0f;
    }
}