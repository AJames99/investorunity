﻿using R2API;
using System;

namespace Investor.Modules
{
    internal static class Tokens
    {
        // Used everywhere within tokens. Format is DeveloperPrefix + BodyPrefix + unique per token
        // A full example token for ThunderHenry would be ROBVALE_THUNDERHENRY_BODY_UNLOCK_SURVIVOR_NAME.
        internal const string investorPrefix = "_INVESTOR_BODY_";

        internal static void Init()
        {
            #region The Investor
            string prefix = InvestorPlugin.developerPrefix + investorPrefix;

            string desc = "Investor is a sly stockholder that manipulates the markets to manipulate the battlefield.<color=#CCD3E0>" + Environment.NewLine + Environment.NewLine;
            desc = desc + "< ! > Spare Change is a powerful close-to-mid range attack, but struggles to attack distant enemies." + Environment.NewLine + Environment.NewLine;
            desc = desc + "< ! > Lobby is best used when overwhelmed by monsters and without the AoE damage to take them on." + Environment.NewLine + Environment.NewLine;
            desc = desc + "< ! > Perform Tax Evasion when you predict a high damage attack by a powerful enemy to maximise your returns." + Environment.NewLine + Environment.NewLine;
            desc = desc + "< ! > Invest can decimate your net worth if you're not careful, so be sure to stock up on mobility tools and damage mitigation." + Environment.NewLine + Environment.NewLine;

            string outro = "..and so he liquidated his assets, searching for a new corner of the market to dominate.";
            string outroFailure = "..and so he filed for bankruptcy, never to trade again.";

            LanguageAPI.Add(prefix + "NAME", "Investor");
            LanguageAPI.Add(prefix + "DESCRIPTION", desc);
            LanguageAPI.Add(prefix + "SUBTITLE", "The Venture Capitalist");
            LanguageAPI.Add(prefix + "LORE", "Exiled from Ganymede's capitalist trading colony WALL-STREET 7 for insider trading and on the run from debt collectors, Investor was forced to fend for himself on backwater planets in hopes of one day repaying his debts and being allowed to own publicly traded stocks once more.");
            LanguageAPI.Add(prefix + "OUTRO_FLAVOR", outro);
            LanguageAPI.Add(prefix + "OUTRO_FAILURE", outroFailure);

            #region Skins
            LanguageAPI.Add(prefix + "DEFAULT_SKIN_NAME", "Default");
            LanguageAPI.Add(prefix + "MASTERY_SKIN_NAME", "[[BIG SHOT]]");

            #endregion

            #region Passive
            LanguageAPI.Add(prefix + "PASSIVE_NAME", "Interest");
            LanguageAPI.Add(prefix + "PASSIVE_DESCRIPTION", "Provides gold every 10 seconds to 5% of your net worth..");
            #endregion

            #region Primary
            LanguageAPI.Add(prefix + "PRIMARY_COINSCATTER_NAME", "Spare Change");
            LanguageAPI.Add(prefix + "PRIMARY_COINSCATTER_DESCRIPTION", $"Throw coins for {StaticValues.coinCount}x<style=cIsDamage>{100f * StaticValues.coinScatterDamageCoefficient}% damage</style>. Deals additional damage based on your net worth, relative to the current stage.");
            //LanguageAPI.Add(prefix + "PRIMARY_PUNCH_NAME", "Boxing Gloves");
            //LanguageAPI.Add(prefix + "PRIMARY_PUNCH_DESCRIPTION", Helpers.agilePrefix + $"Punch rapidly for <style=cIsDamage>{100f * 2.4f}% damage</style>. <style=cIsUtility>Ignores armor.</style>");
            #endregion

            #region Secondary
            LanguageAPI.Add(prefix + "SECONDARY_LOBBY_NAME", "Lobby");
            LanguageAPI.Add(prefix + "SECONDARY_LOBBY_DESCRIPTION", Helpers.agilePrefix + "Spend 10% of your net worth to deal 5000% to all visible enemies.");

            LanguageAPI.Add(prefix + "SECONDARY_EMPLOY_NAME", "Employ");
            LanguageAPI.Add(prefix + "SECONDARY_EMPLOY_DESCRIPTION", Helpers.agilePrefix + "Hold to select an enemy to hire and see their hiring cost. Release to pay the shown amount and hire the target onto your team. Can be used on existing employees to heal them to 100% health. Hired allies gain a copy of all your items.");
            #endregion

            #region Utility
            LanguageAPI.Add(prefix + "UTILITY_TAXEVASION_NAME", "Tax Evasion");
            LanguageAPI.Add(prefix + "UTILITY_TAXEVASION_DESCRIPTION", $"Move faster for {(int)StaticValues.taxEvasionDuration} seconds, <style=cIsUtility>gaining net worth proportional to the amount of raw damage taken</style> when the ability ends. <style=cIsUtility>Summons all your current employees to your aid.</style>.");
            #endregion

            #region Special
            LanguageAPI.Add(prefix + "SPECIAL_INVEST_NAME", "Invest");
            LanguageAPI.Add(prefix + "SPECIAL_INVEST_DESCRIPTION", "Spend half your current net worth to buy a stock. The stock's value decreases over time and when you are damaged, but appreciates in value for the damage you deal and the monsters you kill. After 10 seconds you automatically sell, receiving gold equal to the stock's value.");
            #endregion

            #region Achievements
            //LanguageAPI.Add(prefix + "UNLOCK_SURVIVOR_NAME", "Thunderous Prelude");
            //LanguageAPI.Add(prefix + "UNLOCK_SURVIVOR_DESC", "Enter Distant Roost.");
            //LanguageAPI.Add(prefix + "UNLOCK_SURVIVOR_UNLOCKABLE_NAME", "Thunderous Prelude");

            LanguageAPI.Add(prefix + "UNLOCK_MASTERY_NAME", "[[BIG SHOT]]");
            LanguageAPI.Add(prefix + "UNLOCK_MASTERY_DESC", "As Investor, beat the game or obliterate on Monsoon with a net worth of at least 5,000,000 gold.");
            LanguageAPI.Add(prefix + "UNLOCK_MASTERY_UNLOCKABLE_NAME", "[[BIG SHOT]] Investor");

            //LanguageAPI.Add(prefix + "UNLOCK_UZI_NAME", "Thunder Henry: Shoot 'em up");
            //LanguageAPI.Add(prefix + "UNLOCK_UZI_DESC", "As Thunder Henry, reach +250% attack speed.");
            //LanguageAPI.Add(prefix + "UNLOCK_UZI_UNLOCKABLE_NAME", "Thunder Henry: Shoot 'em up");
            #endregion
            #endregion
        }
    }
}