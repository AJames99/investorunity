﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EntityStates;

namespace RoR2.Skills {

	[CreateAssetMenu(menuName = "RoR2/SkillDef/Stepped")]
	public class SteppedSkillDefInstanceable : SteppedSkillDef {

	}
}