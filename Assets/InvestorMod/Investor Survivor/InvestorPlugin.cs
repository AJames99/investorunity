﻿using BepInEx;
using R2API.Utils;
using RoR2;
using System.Collections.Generic;
using System.Security;
using System.Linq;
using System.Security.Permissions;
using StubbedConverter;
using UnityEngine;
using System;
using RoR2.ContentManagement;
using System.IO;

[module: UnverifiableCode]
[assembly: SecurityPermission(SecurityAction.RequestMinimum, SkipVerification = true)]

//Do a 'Find and Replace' on the ThunderHenry namespace. Make your own namespace, please.
namespace Investor {
	[BepInDependency("com.bepis.r2api", BepInDependency.DependencyFlags.HardDependency)]
	[BepInDependency("com.valex.ShaderConverter", BepInDependency.DependencyFlags.HardDependency)]
	[BepInDependency("com.TeamMoonstorm.MoonstormSharedUtils", BepInDependency.DependencyFlags.HardDependency)]
	[NetworkCompatibility(CompatibilityLevel.EveryoneMustHaveMod, VersionStrictness.EveryoneNeedSameModVersion)]
	[BepInPlugin(MODUID, MODNAME, MODVERSION)]
	[R2APISubmoduleDependency(new string[]
	{
		"PrefabAPI",
		"LanguageAPI",
		"SoundAPI",
	})]
	public class InvestorPlugin : BaseUnityPlugin {
		// if you don't change these you're giving permission to deprecate the mod-
		//  please change the names to your own stuff, thanks
		//   this shouldn't even have to be said
		public const string MODUID = "com.mynachmelyn.Investor";
		public const string MODNAME = "Investor";
		public const string MODVERSION = "1.0.0";

		// a prefix for name tokens to prevent conflicts- please capitalize all name tokens for convention
		public const string developerPrefix = "MYNACHMELYN";

		// use this to toggle debug on stuff, make sure it's false before releasing
		public static bool debug = true;

		public static bool cancel;

		public static InvestorPlugin instance;

		private void Awake() {
			instance = this;

			// Load/Configure assets and read Config
			Modules.Config.ReadConfig();
			Modules.Assets.Init();
			if (cancel) return;
			Modules.Tokens.Init();
			Modules.Prefabs.Init();
			//Modules.Buffs.Init(); // TODO
			Modules.ItemDisplays.Init();
			Modules.Unlockables.Init();


			// Any debug stuff you need to do can go here before initialisation
			if (debug) { Modules.Helpers.AwakeDebug(); }

			//Initialize Content Pack
			Modules.ContentPackProvider.Initialize();

			Hook();
		}

		private static List<Material> staticMaterials = new List<Material>();
		private static void SwapShaders(List<Material> materials) {
			var cloudMat = Resources.Load<GameObject>("Prefabs/Effects/OrbEffects/LightningStrikeOrbEffect").transform.Find("Ring").GetComponent<ParticleSystemRenderer>().material;
			materials.ForEach(Material => {
				if (Material.shader.name.StartsWith("StubbedShader")) {
					Material.shader = Resources.Load<Shader>("shaders" + Material.shader.name.Substring(13));
					if (Material.shader.name.Contains("Cloud Remap")) {
						var eatShit = new RuntimeCloudMaterialMapper(Material); // lol
						Material.CopyPropertiesFromMaterial(cloudMat);
						eatShit.SetMaterialValues(ref Material);
						//HG.ArrayUtils.ArrayAppend(ref cloudRemaps, Material);
						Debug.Log($"### Copied properties of stubbed material of name {Material.shader.name}");
					}
					staticMaterials.Add(Material);
				}
			});
		}

		private void Start() {
			// Using StubbedShaderConverter to convert stubbed materials into Hopoo equivalents.
			//ShaderConvert.ConvertAssetBundleShaders(Modules.Assets.mainAssetBundle, true, debug);		// old doesnt really work
			ShaderConverter.ConvertStubbedShaders(Modules.Assets.mainAssetBundle, true);				// new, works with SSC sttbs

			//new, from LIT
			//SwapShaders(Modules.Assets.mainAssetBundle.LoadAllAssets<Material>().ToList()); // causes mats to be totally fucked
		}

		private void Hook() {
			On.RoR2.CharacterBody.RecalculateStats += CharacterBody_RecalculateStats;
		}

		private void CharacterBody_RecalculateStats(On.RoR2.CharacterBody.orig_RecalculateStats orig, CharacterBody self) {
			orig(self);

			// a simple stat hook, adds armor after stats are recalculated
			if (self) {
			}
		}
	}
}
